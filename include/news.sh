# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2017,2021-2023 Leah Rowe <leah@libreboot.org>
# SPDX-FileCopyrightText: 2017 Alyssa Rosenzweig <alyssa@rosenzweig.io>
# SPDX-FileCopyrightText: 2017 Michael Reed <michael@michaelreed.io>

mknews()
{
	find -L "${1}/site/" -type f -name "MANIFEST" > "${tmpdir}/xmanifest"
	while read -r y; do
		check_path f "${y}" || continue
		mkarticle "${1}" "${y}"
	done < "${tmpdir}/xmanifest"
}

mkarticle()
{
	_sitedir="${1}"
	y="${2}"
	[ -f "${y}" ] || return 0

	eval "$(setvars "News" BLOGTITLE BLOGDESCRIPTION)"
	_manifestdir="${y##"${_sitedir}/site/"}"
	_manifestdir="${_manifestdir%MANIFEST}"
	_manifestdir="${_manifestdir%/}"

	check_path f "$_sitedir/site/${_manifestdir}/news-list.md.include" || \
	    return 0

	newspages "${_sitedir}/site/${_manifestdir}" "${y}" > "${tmpdir}/xnews"
	while read -r f; do
		check_path f "${f}" || return 0
	done < "${tmpdir}/xnews"

	eval "$(getConfigValues \
	    "${_sitedir}/site/${_manifestdir}/news.cfg" \
	    BLOGTITLE BLOGDESCRIPTION)"

	[ -z "${BLOGTITLE}" ] && BLOGTITLE="News"
	[ -z "${BLOGDESCRIPTION}" ] && BLOGDESCRIPTION="News"

	# generate the index file
	cat "$_sitedir/site/${_manifestdir}/news-list.md.include" \
	    > "${_sitedir}/site/${_manifestdir}/index.md"
	if [ "${_manifestdir}" = "${BLOGDIR%/}" ]; then
		printf "\nSubscribe to RSS: [/feed.xml](/feed.xml)\n\n" \
		    >> "${_sitedir}/site/${_manifestdir}/index.md"
	else
		printf "\nSubscribe to RSS: [/%sfeed.xml](/%sfeed.xml)\n\n" \
		    "${_manifestdir}" "${_manifestdir}" \
		    >> "${_sitedir}/site/${_manifestdir}/index.md"
	fi
	while read -r f; do
		_page="$(sanitizefilename "${f#"${_sitedir}/site/"}")"
		meta "${_page}" "${_sitedir}/site" \
		    >> "${_sitedir}/site/${_manifestdir}/index.md"
	done < "${tmpdir}/xnews"

	# generate the RSS index
	rss_header "$BLOGTITLE" "$DOMAIN" "$BLOGDESCRIPTION" "$_manifestdir" \
	    > "${_sitedir}/site/${_manifestdir}/feed.xml"
	while read -r f; do
		_page="$(sanitizefilename "${f#"${_sitedir}/site/"}")"
		rss_main "$_page" "$_sitedir" "$DOMAIN" \
		    >> "${_sitedir}/site/${_manifestdir}/feed.xml"
	done < "${tmpdir}/xnews"
	rss_footer >> "${_sitedir}/site/${_manifestdir}/feed.xml"

	if [ "${_sitedir}/site/${_manifestdir}/feed.xml" \
	    != "${_sitedir}/site/feed.xml" ] && \
	    [ "${_manifestdir}" = "${BLOGDIR%/}" ] && \
	    [ -n "${BLOGDIR%/}" ]; then
		rm -f "${_sitedir}/site/feed.xml"
		[ -f "${_sitedir}/site/${_manifestdir}/feed.xml" ] && \
			cp "${_sitedir}/site/${_manifestdir}/feed.xml" \
			    "${_sitedir}/site/feed.xml"
	fi

	mkhtml "$_sitedir/site/${_manifestdir}/index.md" "${_sitedir##*/}"
}

# usage: meta file
meta()
{
	printf '%s\n' \
	"[$(mktitle "${2}/${1}")](/${1}){.title}"

	printf '%s\n' \
	"[$(sed -n 3p "${2}/${1}" | sed -e s-^..--)]{.date}"
	printf "\n\n"
}

# usage: rss_header
rss_header()
{
	_blogtitle="${1}"
	_domain="${2}" # without a / at the end, but with http:// or https://
	_blogdescription="${3}"
	_blog="${4}"

	printf "%s\n" "<rss version=\"2.0\">"
	printf "%s\n" "<channel>"

	printf "%s\n" "<title>${_blogtitle}</title>"
	printf "%s\n" "<link>${_domain}${_blog}</link>"
	printf "%s\n" "<description>$_blogdescription</description>"
}

# usage: rss_main file
rss_main()
{
	_file="${1}"
	_sitedir="${2}"
	_domain="${3}"

	# render content and escape
	_htmlfile="${_file%.md}.html"

	_pagetitle=$(mktitle "${_sitedir}/site/${_file}")
	_pageurl="${_domain}${_htmlfile}"

	printf "%s\n" '<item>'
	printf "%s\n" "<title>${_pagetitle}</title>"
	printf "%s\n" "<link>${_pageurl}</link>"
	printf "%s" "<description><p>Article: ${_pagetitle}</p>"
	printf "%s\n" "<p>Web link: ${_pageurl}</p></description>"
	printf "</item>\n"
}

# usage: title file
mktitle()
{
	_firstchar=$(head -c 1 "${1}")
	_firstthreechars=$(head -c 3 "${1}")
	_titlestr="$(head -n1 "${1}")"
	if [ "${_firstchar}" = "%" ] || [ "${_firstchar}" = "#" ]; then
		_titlestr="$(sed -n 1p "${1}" | sed -e s-^..--)"
	elif [ "${_firstthreechars}" = "---" ]; then
		_titlestr="$(sed -n 2p "${1}" | sed -e s-^..--)"
		_titlestr="${_titlestr#* }"
	fi
	printf "%s\n" "${_titlestr}"
}

# usage: rss_footer
rss_footer()
{
	printf '%s\n' '</channel>'
	printf '%s\n' '</rss>'
}
